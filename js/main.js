/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);

__webpack_require__(2);

__webpack_require__(3);

__webpack_require__(4);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.senapi_bootstrap = {
    attach: function (context, settings) {
      if (!context.activeElement) {
        return false;
      }

      $('.producciones-carousel').owlCarousel({
        loop: true,
        margin: 10,
        //nav: true,
        autoplay: true,
        center: true,
        autoplaySpeed: 2000,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        transitionStyle: 'fade',
        stagePadding: 0,
        mouseDrag: true,
        touchDrag: true,
        dots: true,
        dotClass: 'owl-dot',
        dotsClass: 'owl-dots',
        items: 1,
        autoWidth: 'auto',
        mobileFirst: true,
        responsive: {
          0: {
            items: 1
          },
          400: {
            items: 2
          },
          700: {
            items: 3
          },
          1000: {
            items: 5,
            margin: 20
          }
        }
      });
      pdfjsLib.GlobalWorkerOptions.workerSrc = settings.senapi_bootstrap.pdfWorkerSrc;

      function isCanvasSupported() {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
      }

      console.log(context);
      var hide = false;
      $('#bd-modal-lg').on('show.bs.modal', function (event) {
        event.stopPropagation();
        console.log(event);
        var element = $(event.relatedTarget);
        var url = element.data('url');

        if (!url) {
          url = element.attr('href');
        }

        var modal = $(this);
        modal.append('<div id="control-icon" class="position-fixed fixed-top"><div class="btn-group-vertical">' + '<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>\n' + '<button id="zoom-next" type="button" class="btn btn-secondary"><i class="fa fa-search-plus" aria-hidden="true"></i></button>\n' + '<button id="zoom-prev" type="button" class="btn btn-secondary"><i class="fa fa-search-minus" aria-hidden="true"></i></button>\n' + '<button id="download" type="button" class="btn btn-secondary"><i class="fa fa-external-link" aria-hidden="true"></i></button>\n' + '</div></div>');
        var classes = ['modal-lg', 'modal-lg-70', 'modal-lg-80', 'modal-lg-90', 'modal-lg-100'];
        var currentClass = 1;
        var dialog = modal.find('.modal-dialog');

        for (var i = 0; i < classes.length; i++) {
          if ($(dialog).hasClass(classes[i])) {
            currentClass = i + 1;
            break;
          }
        }

        modal.find('#zoom-next').on('click', function (e) {
          e.stopPropagation();

          if (currentClass < classes.length) {
            $(dialog).removeClass().addClass('modal-dialog ' + classes[currentClass++]);
          }
        });
        modal.find('#zoom-prev').on('click', function (e) {
          e.stopPropagation();

          if (currentClass - 2 >= 0) {
            $(dialog).removeClass().addClass('modal-dialog ' + classes[currentClass - 2]);
            currentClass--;

            if (currentClass < 0) {
              currentClass = 0;
            }
          }
        });
        modal.find('#download').on('click', function (e) {
          e.stopPropagation();
          var a = document.createElement('a');
          a.target = "_blank";
          a.href = url;
          a.click();
        });
        var content = modal.find('.modal-content');
        content.html('');
        var loading = $('<div style="height: 500px;background: white; position: relative;"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
        content.append(loading);

        if (isCanvasSupported()) {
          var currentPage = 1;
          pdfjsLib.getDocument(url).then(function (pdf) {
            hide = false;
            if (currentPage <= pdf.numPages) getPage();

            function getPage() {
              if (hide) {
                currentPage = pdf.numPages;
                content.html();
                return;
              }

              pdf.getPage(currentPage).then(function (page) {
                var scale = 1.5;
                var viewport = page.getViewport(scale);
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                canvas.classList.add('w-100');
                canvas.classList.add('h-100');
                var renderContext = {
                  canvasContext: ctx,
                  viewport: viewport
                };
                page.render(renderContext).then(function () {
                  $(loading).remove();
                  /*var img = new Image;
                   img.onload = function () {
                      ctx.drawImage(this, 0,0, ctx.canvas.width, ctx.canvas.height);
                   };
                   img.src = _canvas.toDataURL();
                   img.classList.add('w-100');
                   img.classList.add('h-100');
                   $(content).append(img);*/

                  $(content).append(canvas);

                  if (currentPage < pdf.numPages) {
                    currentPage++;
                    getPage();
                  }
                });
              });
            }
          });
        }
      });
      $('#bd-modal-lg').on('hidden.bs.modal', function (event) {
        var modal = $(this);
        var content = modal.find('.modal-content');
        modal.find('#control-icon').remove();
        content.html('');
        hide = true;
      });
    }
  };
})(jQuery, Drupal);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

(function ($, _, Drupal, drupalSettings) {
  Drupal.behaviors.sidr_trigger = {
    attach: function (context, drupalSettings) {
      $(context).find('.js-sidr-trigger').once('sidr-trigger').each(function () {
        var $trigger = $(this); //var options = $trigger.attr('data-sidr-options') || '{}';

        var options = '{"source":"#superfish-main","side":"right","method":"toggle","renaming":false,"displace":false,"nocopy":false}';
        options = $.parseJSON(options);
        var $target = $(options.source);

        if ($target.length === 0) {
          Drupal.throwError('Target element not found: ' + options.source);
          return;
        }

        options.onOpenEnd = function () {
          var sidr = this;
        };

        options.onCloseEnd = function () {
          var sidr = this;
        };

        var htmlContent = '',
            idMenu = '#sidr-hidden';
        $.each([options.source], function (index, element) {
          var accordion = $(element).clone();
          accordion.attr('id', 'superfish-main-accordion-hidden');
          accordion.attr('class', 'sf-menu sf-main sf-style-none sf-accordion sf-expanded');
          accordion.removeClass('sf-horizontal sf-vertical sf-navbar sf-shadow sf-js-enabled');
          accordion.find('li').each(function () {
            $(this).removeAttr('style').removeClass('sfHover').attr('id', $(this).attr('id') + '-accordion');
          }); //accordion.children('ul').removeAttr('style').not('.sf-hidden').addClass('sf-hidden');

          var parent = accordion.find('li.menuparent');

          for (var i = 0; i < parent.length; i++) {
            parent.eq(i).children('a').append('<span class="sf-sub-indicator"> »</span>');
            parent.eq(i).children('ul').removeClass('sf-hidden').slideDown('fast').end().addClass('sf-expanded');
          }

          var htmlBtnClose = '<span id="superfish-main-accordion-hidden-close" class="fa fa-times-circle"></span>';
          htmlContent += '<div id="sidr-hidden">' + htmlBtnClose + $("<div />").append(accordion).html() + '</div>';
        });
        $('body').append(htmlContent);
        var menuRef = $(idMenu);
        options.source = idMenu;
        $trigger.sidr(options);
        menuRef.remove();
        var accordionElement = $('#superfish-main-accordion-hidden'),
            button = accordionElement.find('a.menuparent,span.nolink.menuparent');
        accordionElement.addClass('sf-expanded').hide().removeClass('sf-hidden').show();
        button.on('click', function (e) {
          if ($(this).closest('li').children('ul').length > 0) {
            e.preventDefault();
            var parent = $(this).closest('li');
            /**if (parent.children('a.menuparent,span.nolink.menuparent').length > 0 && parent.children('ul').children('li.sf-clone-parent').length == 0) {
              var cloneLink = parent.children('a.menuparent,span.nolink.menuparent').clone();
              cloneLink.removeClass('menuparent sf-depth-1').children('.sf-sub-indicator').remove();
              cloneLink = $('<li class="sf-clone-parent" />').html(cloneLink);
              parent.children('ul').addClass('sf-has-clone-parent').prepend(cloneLink);
            }*/

            if (parent.hasClass('sf-expanded')) {
              parent.children('ul').slideUp('fast', function () {
                $(this).closest('li').removeClass('sf-expanded').end().addClass('sf-hidden').show();
              });
            } else {
              parent.children('ul').hide().removeClass('sf-hidden').slideDown('fast').end().addClass('sf-expanded')
              /*.children('a.sf-accordion-button')
              .end().siblings('li.sf-expanded').children('ul')
              .slideUp('fast', function () {
              $(this).closest('li').removeClass('sf-expanded').end().addClass('sf-hidden').show();
              })*/
              ;
            }
          }
        });
        var sidrId = $trigger.data('sidr');
        var $sidr = $('#' + sidrId);
        var btnClose = $('#superfish-main-accordion-hidden-close');
        btnClose.on('click', function (e) {
          e.preventDefault(); //alert("clickme");

          $.sidr('close', jQuery.sidr('status').opened);
        });
        $trigger.attr('aria-controls', sidrId).attr('aria-expanded', false);

        if (options.nocopy && $target.length > 0) {
          var $inner = $('<div class="sidr-inner"></div>').append($target);
          $sidr.html($inner);
        }

        $trigger.click(function () {
          $(document.body).data('sidr.lastTrigger', this);
        });
      });
      $(document.body).once('sidr-unfocus').bind('click keyup', function (e) {
        var openSidr = jQuery.sidr('status').opened;

        if (!openSidr) {
          return;
        }

        var isBlur = true;

        if ($(e.target).closest('.sidr').length !== 0) {
          isBlur = false;
        }

        if ($(e.target).closest('.js-sidr-trigger').length !== 0) {
          isBlur = false;
        }

        if (e.type === 'keyup' && e.keyCode == 27) {
          isBlur = true;
        }

        if (isBlur) {
          $.sidr('close', openSidr);

          if (e.type === 'keyup') {
            var lastTrigger = $(document.body).data('sidr.lastTrigger');

            if (lastTrigger) {
              $(lastTrigger).focus();
            }
          }
        }
      });
    }
  };
})(window.jQuery, window._, window.Drupal, window.drupalSettings);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function ($) {
  $(window).on('load', function () {
    setTimeout(function () {
      $('.loader-live').fadeOut();
    }, 1000);
  });
})(jQuery);

/***/ }),
/* 4 */
/***/ (function(module, exports) {

(function ($) {
  "use strict";

  $(document).ready(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
      } else {
        $('.scrollup').fadeOut();
      }
    });
    $('.scrollup').on("click", function () {
      $("html, body").animate({
        scrollTop: 0
      }, 500);
      return false;
    });
  });
})(jQuery);

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map